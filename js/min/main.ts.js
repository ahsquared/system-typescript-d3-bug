System.register("d3-selection/src/namespaces", [], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var xhtml;
    return {
        setters: [],
        execute: function () {
            exports_1("xhtml", xhtml = "http://www.w3.org/1999/xhtml");
            exports_1("default", {
                svg: "http://www.w3.org/2000/svg",
                xhtml: xhtml,
                xlink: "http://www.w3.org/1999/xlink",
                xml: "http://www.w3.org/XML/1998/namespace",
                xmlns: "http://www.w3.org/2000/xmlns/"
            });
        }
    };
});
System.register("d3-selection/src/namespace", ["d3-selection/src/namespaces"], function (exports_2, context_2) {
    "use strict";
    var __moduleName = context_2 && context_2.id;
    function default_1(name) {
        var prefix = name += "", i = prefix.indexOf(":");
        if (i >= 0 && (prefix = name.slice(0, i)) !== "xmlns")
            name = name.slice(i + 1);
        return namespaces_1.default.hasOwnProperty(prefix) ? { space: namespaces_1.default[prefix], local: name } : name;
    }
    exports_2("default", default_1);
    var namespaces_1;
    return {
        setters: [
            function (namespaces_1_1) {
                namespaces_1 = namespaces_1_1;
            }
        ],
        execute: function () {
        }
    };
});
System.register("d3-selection/src/creator", ["d3-selection/src/namespace", "d3-selection/src/namespaces"], function (exports_3, context_3) {
    "use strict";
    var __moduleName = context_3 && context_3.id;
    function creatorInherit(name) {
        return function () {
            var document = this.ownerDocument, uri = this.namespaceURI;
            return uri === namespaces_2.xhtml && document.documentElement.namespaceURI === namespaces_2.xhtml
                ? document.createElement(name)
                : document.createElementNS(uri, name);
        };
    }
    function creatorFixed(fullname) {
        return function () {
            return this.ownerDocument.createElementNS(fullname.space, fullname.local);
        };
    }
    function default_2(name) {
        var fullname = namespace_1.default(name);
        return (fullname.local
            ? creatorFixed
            : creatorInherit)(fullname);
    }
    exports_3("default", default_2);
    var namespace_1, namespaces_2;
    return {
        setters: [
            function (namespace_1_1) {
                namespace_1 = namespace_1_1;
            },
            function (namespaces_2_1) {
                namespaces_2 = namespaces_2_1;
            }
        ],
        execute: function () {
        }
    };
});
System.register("d3-selection/src/local", [], function (exports_4, context_4) {
    "use strict";
    var __moduleName = context_4 && context_4.id;
    function local() {
        return new Local;
    }
    exports_4("default", local);
    function Local() {
        this._ = "@" + (++nextId).toString(36);
    }
    var nextId;
    return {
        setters: [],
        execute: function () {
            nextId = 0;
            Local.prototype = local.prototype = {
                constructor: Local,
                get: function (node) {
                    var id = this._;
                    while (!(id in node))
                        if (!(node = node.parentNode))
                            return;
                    return node[id];
                },
                set: function (node, value) {
                    return node[this._] = value;
                },
                remove: function (node) {
                    return this._ in node && delete node[this._];
                },
                toString: function () {
                    return this._;
                }
            };
        }
    };
});
System.register("d3-selection/src/matcher", [], function (exports_5, context_5) {
    "use strict";
    var __moduleName = context_5 && context_5.id;
    var matcher;
    return {
        setters: [],
        execute: function () {
            matcher = function (selector) {
                return function () {
                    return this.matches(selector);
                };
            };
            if (typeof document !== "undefined") {
                var element = document.documentElement;
                if (!element.matches) {
                    var vendorMatches = element.webkitMatchesSelector
                        || element.msMatchesSelector
                        || element.mozMatchesSelector
                        || element.oMatchesSelector;
                    matcher = function (selector) {
                        return function () {
                            return vendorMatches.call(this, selector);
                        };
                    };
                }
            }
            exports_5("default", matcher);
        }
    };
});
System.register("d3-selection/src/selection/on", [], function (exports_6, context_6) {
    "use strict";
    var __moduleName = context_6 && context_6.id;
    function filterContextListener(listener, index, group) {
        listener = contextListener(listener, index, group);
        return function (event) {
            var related = event.relatedTarget;
            if (!related || (related !== this && !(related.compareDocumentPosition(this) & 8))) {
                listener.call(this, event);
            }
        };
    }
    function contextListener(listener, index, group) {
        return function (event1) {
            var event0 = event; // Events can be reentrant (e.g., focus).
            exports_6("event", // Events can be reentrant (e.g., focus).
            event = event1);
            try {
                listener.call(this, this.__data__, index, group);
            }
            finally {
                exports_6("event", event = event0);
            }
        };
    }
    function parseTypenames(typenames) {
        return typenames.trim().split(/^|\s+/).map(function (t) {
            var name = "", i = t.indexOf(".");
            if (i >= 0)
                name = t.slice(i + 1), t = t.slice(0, i);
            return { type: t, name: name };
        });
    }
    function onRemove(typename) {
        return function () {
            var on = this.__on;
            if (!on)
                return;
            for (var j = 0, i = -1, m = on.length, o; j < m; ++j) {
                if (o = on[j], (!typename.type || o.type === typename.type) && o.name === typename.name) {
                    this.removeEventListener(o.type, o.listener, o.capture);
                }
                else {
                    on[++i] = o;
                }
            }
            if (++i)
                on.length = i;
            else
                delete this.__on;
        };
    }
    function onAdd(typename, value, capture) {
        var wrap = filterEvents.hasOwnProperty(typename.type) ? filterContextListener : contextListener;
        return function (d, i, group) {
            var on = this.__on, o, listener = wrap(value, i, group);
            if (on)
                for (var j = 0, m = on.length; j < m; ++j) {
                    if ((o = on[j]).type === typename.type && o.name === typename.name) {
                        this.removeEventListener(o.type, o.listener, o.capture);
                        this.addEventListener(o.type, o.listener = listener, o.capture = capture);
                        o.value = value;
                        return;
                    }
                }
            this.addEventListener(typename.type, listener, capture);
            o = { type: typename.type, name: typename.name, value: value, listener: listener, capture: capture };
            if (!on)
                this.__on = [o];
            else
                on.push(o);
        };
    }
    function default_3(typename, value, capture) {
        var typenames = parseTypenames(typename + ""), i, n = typenames.length, t;
        if (arguments.length < 2) {
            var on = this.node().__on;
            if (on)
                for (var j = 0, m = on.length, o; j < m; ++j) {
                    for (i = 0, o = on[j]; i < n; ++i) {
                        if ((t = typenames[i]).type === o.type && t.name === o.name) {
                            return o.value;
                        }
                    }
                }
            return;
        }
        on = value ? onAdd : onRemove;
        if (capture == null)
            capture = false;
        for (i = 0; i < n; ++i)
            this.each(on(typenames[i], value, capture));
        return this;
    }
    exports_6("default", default_3);
    function customEvent(event1, listener, that, args) {
        var event0 = event;
        event1.sourceEvent = event;
        exports_6("event", event = event1);
        try {
            return listener.apply(that, args);
        }
        finally {
            exports_6("event", event = event0);
        }
    }
    exports_6("customEvent", customEvent);
    var filterEvents, event;
    return {
        setters: [],
        execute: function () {
            filterEvents = {};
            exports_6("event", event = null);
            if (typeof document !== "undefined") {
                var element = document.documentElement;
                if (!("onmouseenter" in element)) {
                    filterEvents = { mouseenter: "mouseover", mouseleave: "mouseout" };
                }
            }
        }
    };
});
System.register("d3-selection/src/sourceEvent", ["d3-selection/src/selection/on"], function (exports_7, context_7) {
    "use strict";
    var __moduleName = context_7 && context_7.id;
    function default_4() {
        var current = on_1.event, source;
        while (source = current.sourceEvent)
            current = source;
        return current;
    }
    exports_7("default", default_4);
    var on_1;
    return {
        setters: [
            function (on_1_1) {
                on_1 = on_1_1;
            }
        ],
        execute: function () {
        }
    };
});
System.register("d3-selection/src/point", [], function (exports_8, context_8) {
    "use strict";
    var __moduleName = context_8 && context_8.id;
    function default_5(node, event) {
        var svg = node.ownerSVGElement || node;
        if (svg.createSVGPoint) {
            var point = svg.createSVGPoint();
            point.x = event.clientX, point.y = event.clientY;
            point = point.matrixTransform(node.getScreenCTM().inverse());
            return [point.x, point.y];
        }
        var rect = node.getBoundingClientRect();
        return [event.clientX - rect.left - node.clientLeft, event.clientY - rect.top - node.clientTop];
    }
    exports_8("default", default_5);
    return {
        setters: [],
        execute: function () {
        }
    };
});
System.register("d3-selection/src/mouse", ["d3-selection/src/sourceEvent", "d3-selection/src/point"], function (exports_9, context_9) {
    "use strict";
    var __moduleName = context_9 && context_9.id;
    function default_6(node) {
        var event = sourceEvent_1.default();
        if (event.changedTouches)
            event = event.changedTouches[0];
        return point_1.default(node, event);
    }
    exports_9("default", default_6);
    var sourceEvent_1, point_1;
    return {
        setters: [
            function (sourceEvent_1_1) {
                sourceEvent_1 = sourceEvent_1_1;
            },
            function (point_1_1) {
                point_1 = point_1_1;
            }
        ],
        execute: function () {
        }
    };
});
System.register("d3-selection/src/selector", [], function (exports_10, context_10) {
    "use strict";
    var __moduleName = context_10 && context_10.id;
    function none() { }
    function default_7(selector) {
        return selector == null ? none : function () {
            return this.querySelector(selector);
        };
    }
    exports_10("default", default_7);
    return {
        setters: [],
        execute: function () {
        }
    };
});
System.register("d3-selection/src/selection/select", ["d3-selection/src/selection/index", "d3-selection/src/selector"], function (exports_11, context_11) {
    "use strict";
    var __moduleName = context_11 && context_11.id;
    function default_8(select) {
        if (typeof select !== "function")
            select = selector_1.default(select);
        for (var groups = this._groups, m = groups.length, subgroups = new Array(m), j = 0; j < m; ++j) {
            for (var group = groups[j], n = group.length, subgroup = subgroups[j] = new Array(n), node, subnode, i = 0; i < n; ++i) {
                if ((node = group[i]) && (subnode = select.call(node, node.__data__, i, group))) {
                    if ("__data__" in node)
                        subnode.__data__ = node.__data__;
                    subgroup[i] = subnode;
                }
            }
        }
        return new index_1.Selection(subgroups, this._parents);
    }
    exports_11("default", default_8);
    var index_1, selector_1;
    return {
        setters: [
            function (index_1_1) {
                index_1 = index_1_1;
            },
            function (selector_1_1) {
                selector_1 = selector_1_1;
            }
        ],
        execute: function () {
        }
    };
});
System.register("d3-selection/src/selectorAll", [], function (exports_12, context_12) {
    "use strict";
    var __moduleName = context_12 && context_12.id;
    function empty() {
        return [];
    }
    function default_9(selector) {
        return selector == null ? empty : function () {
            return this.querySelectorAll(selector);
        };
    }
    exports_12("default", default_9);
    return {
        setters: [],
        execute: function () {
        }
    };
});
System.register("d3-selection/src/selection/selectAll", ["d3-selection/src/selection/index", "d3-selection/src/selectorAll"], function (exports_13, context_13) {
    "use strict";
    var __moduleName = context_13 && context_13.id;
    function default_10(select) {
        if (typeof select !== "function")
            select = selectorAll_1.default(select);
        for (var groups = this._groups, m = groups.length, subgroups = [], parents = [], j = 0; j < m; ++j) {
            for (var group = groups[j], n = group.length, node, i = 0; i < n; ++i) {
                if (node = group[i]) {
                    subgroups.push(select.call(node, node.__data__, i, group));
                    parents.push(node);
                }
            }
        }
        return new index_2.Selection(subgroups, parents);
    }
    exports_13("default", default_10);
    var index_2, selectorAll_1;
    return {
        setters: [
            function (index_2_1) {
                index_2 = index_2_1;
            },
            function (selectorAll_1_1) {
                selectorAll_1 = selectorAll_1_1;
            }
        ],
        execute: function () {
        }
    };
});
System.register("d3-selection/src/selection/filter", ["d3-selection/src/selection/index", "d3-selection/src/matcher"], function (exports_14, context_14) {
    "use strict";
    var __moduleName = context_14 && context_14.id;
    function default_11(match) {
        if (typeof match !== "function")
            match = matcher_1.default(match);
        for (var groups = this._groups, m = groups.length, subgroups = new Array(m), j = 0; j < m; ++j) {
            for (var group = groups[j], n = group.length, subgroup = subgroups[j] = [], node, i = 0; i < n; ++i) {
                if ((node = group[i]) && match.call(node, node.__data__, i, group)) {
                    subgroup.push(node);
                }
            }
        }
        return new index_3.Selection(subgroups, this._parents);
    }
    exports_14("default", default_11);
    var index_3, matcher_1;
    return {
        setters: [
            function (index_3_1) {
                index_3 = index_3_1;
            },
            function (matcher_1_1) {
                matcher_1 = matcher_1_1;
            }
        ],
        execute: function () {
        }
    };
});
System.register("d3-selection/src/selection/sparse", [], function (exports_15, context_15) {
    "use strict";
    var __moduleName = context_15 && context_15.id;
    function default_12(update) {
        return new Array(update.length);
    }
    exports_15("default", default_12);
    return {
        setters: [],
        execute: function () {
        }
    };
});
System.register("d3-selection/src/selection/enter", ["d3-selection/src/selection/sparse", "d3-selection/src/selection/index"], function (exports_16, context_16) {
    "use strict";
    var __moduleName = context_16 && context_16.id;
    function default_13() {
        return new index_4.Selection(this._enter || this._groups.map(sparse_1.default), this._parents);
    }
    exports_16("default", default_13);
    function EnterNode(parent, datum) {
        this.ownerDocument = parent.ownerDocument;
        this.namespaceURI = parent.namespaceURI;
        this._next = null;
        this._parent = parent;
        this.__data__ = datum;
    }
    exports_16("EnterNode", EnterNode);
    var sparse_1, index_4;
    return {
        setters: [
            function (sparse_1_1) {
                sparse_1 = sparse_1_1;
            },
            function (index_4_1) {
                index_4 = index_4_1;
            }
        ],
        execute: function () {
            EnterNode.prototype = {
                constructor: EnterNode,
                appendChild: function (child) { return this._parent.insertBefore(child, this._next); },
                insertBefore: function (child, next) { return this._parent.insertBefore(child, next); },
                querySelector: function (selector) { return this._parent.querySelector(selector); },
                querySelectorAll: function (selector) { return this._parent.querySelectorAll(selector); }
            };
        }
    };
});
System.register("d3-selection/src/constant", [], function (exports_17, context_17) {
    "use strict";
    var __moduleName = context_17 && context_17.id;
    function default_14(x) {
        return function () {
            return x;
        };
    }
    exports_17("default", default_14);
    return {
        setters: [],
        execute: function () {
        }
    };
});
System.register("d3-selection/src/selection/data", ["d3-selection/src/selection/index", "d3-selection/src/selection/enter", "d3-selection/src/constant"], function (exports_18, context_18) {
    "use strict";
    var __moduleName = context_18 && context_18.id;
    function bindIndex(parent, group, enter, update, exit, data) {
        var i = 0, node, groupLength = group.length, dataLength = data.length;
        // Put any non-null nodes that fit into update.
        // Put any null nodes into enter.
        // Put any remaining data into enter.
        for (; i < dataLength; ++i) {
            if (node = group[i]) {
                node.__data__ = data[i];
                update[i] = node;
            }
            else {
                enter[i] = new enter_1.EnterNode(parent, data[i]);
            }
        }
        // Put any non-null nodes that don’t fit into exit.
        for (; i < groupLength; ++i) {
            if (node = group[i]) {
                exit[i] = node;
            }
        }
    }
    function bindKey(parent, group, enter, update, exit, data, key) {
        var i, node, nodeByKeyValue = {}, groupLength = group.length, dataLength = data.length, keyValues = new Array(groupLength), keyValue;
        // Compute the key for each node.
        // If multiple nodes have the same key, the duplicates are added to exit.
        for (i = 0; i < groupLength; ++i) {
            if (node = group[i]) {
                keyValues[i] = keyValue = keyPrefix + key.call(node, node.__data__, i, group);
                if (keyValue in nodeByKeyValue) {
                    exit[i] = node;
                }
                else {
                    nodeByKeyValue[keyValue] = node;
                }
            }
        }
        // Compute the key for each datum.
        // If there a node associated with this key, join and add it to update.
        // If there is not (or the key is a duplicate), add it to enter.
        for (i = 0; i < dataLength; ++i) {
            keyValue = keyPrefix + key.call(parent, data[i], i, data);
            if (node = nodeByKeyValue[keyValue]) {
                update[i] = node;
                node.__data__ = data[i];
                nodeByKeyValue[keyValue] = null;
            }
            else {
                enter[i] = new enter_1.EnterNode(parent, data[i]);
            }
        }
        // Add any remaining nodes that were not bound to data to exit.
        for (i = 0; i < groupLength; ++i) {
            if ((node = group[i]) && (nodeByKeyValue[keyValues[i]] === node)) {
                exit[i] = node;
            }
        }
    }
    function default_15(value, key) {
        if (!value) {
            data = new Array(this.size()), j = -1;
            this.each(function (d) { data[++j] = d; });
            return data;
        }
        var bind = key ? bindKey : bindIndex, parents = this._parents, groups = this._groups;
        if (typeof value !== "function")
            value = constant_1.default(value);
        for (var m = groups.length, update = new Array(m), enter = new Array(m), exit = new Array(m), j = 0; j < m; ++j) {
            var parent = parents[j], group = groups[j], groupLength = group.length, data = value.call(parent, parent && parent.__data__, j, parents), dataLength = data.length, enterGroup = enter[j] = new Array(dataLength), updateGroup = update[j] = new Array(dataLength), exitGroup = exit[j] = new Array(groupLength);
            bind(parent, group, enterGroup, updateGroup, exitGroup, data, key);
            // Now connect the enter nodes to their following update node, such that
            // appendChild can insert the materialized enter node before this node,
            // rather than at the end of the parent node.
            for (var i0 = 0, i1 = 0, previous, next; i0 < dataLength; ++i0) {
                if (previous = enterGroup[i0]) {
                    if (i0 >= i1)
                        i1 = i0 + 1;
                    while (!(next = updateGroup[i1]) && ++i1 < dataLength)
                        ;
                    previous._next = next || null;
                }
            }
        }
        update = new index_5.Selection(update, parents);
        update._enter = enter;
        update._exit = exit;
        return update;
    }
    exports_18("default", default_15);
    var index_5, enter_1, constant_1, keyPrefix;
    return {
        setters: [
            function (index_5_1) {
                index_5 = index_5_1;
            },
            function (enter_1_1) {
                enter_1 = enter_1_1;
            },
            function (constant_1_1) {
                constant_1 = constant_1_1;
            }
        ],
        execute: function () {
            keyPrefix = "$"; // Protect against keys like “__proto__”.
        }
    };
});
System.register("d3-selection/src/selection/exit", ["d3-selection/src/selection/sparse", "d3-selection/src/selection/index"], function (exports_19, context_19) {
    "use strict";
    var __moduleName = context_19 && context_19.id;
    function default_16() {
        return new index_6.Selection(this._exit || this._groups.map(sparse_2.default), this._parents);
    }
    exports_19("default", default_16);
    var sparse_2, index_6;
    return {
        setters: [
            function (sparse_2_1) {
                sparse_2 = sparse_2_1;
            },
            function (index_6_1) {
                index_6 = index_6_1;
            }
        ],
        execute: function () {
        }
    };
});
System.register("d3-selection/src/selection/merge", ["d3-selection/src/selection/index"], function (exports_20, context_20) {
    "use strict";
    var __moduleName = context_20 && context_20.id;
    function default_17(selection) {
        for (var groups0 = this._groups, groups1 = selection._groups, m0 = groups0.length, m1 = groups1.length, m = Math.min(m0, m1), merges = new Array(m0), j = 0; j < m; ++j) {
            for (var group0 = groups0[j], group1 = groups1[j], n = group0.length, merge = merges[j] = new Array(n), node, i = 0; i < n; ++i) {
                if (node = group0[i] || group1[i]) {
                    merge[i] = node;
                }
            }
        }
        for (; j < m0; ++j) {
            merges[j] = groups0[j];
        }
        return new index_7.Selection(merges, this._parents);
    }
    exports_20("default", default_17);
    var index_7;
    return {
        setters: [
            function (index_7_1) {
                index_7 = index_7_1;
            }
        ],
        execute: function () {
        }
    };
});
System.register("d3-selection/src/selection/order", [], function (exports_21, context_21) {
    "use strict";
    var __moduleName = context_21 && context_21.id;
    function default_18() {
        for (var groups = this._groups, j = -1, m = groups.length; ++j < m;) {
            for (var group = groups[j], i = group.length - 1, next = group[i], node; --i >= 0;) {
                if (node = group[i]) {
                    if (next && next !== node.nextSibling)
                        next.parentNode.insertBefore(node, next);
                    next = node;
                }
            }
        }
        return this;
    }
    exports_21("default", default_18);
    return {
        setters: [],
        execute: function () {
        }
    };
});
System.register("d3-selection/src/selection/sort", ["d3-selection/src/selection/index"], function (exports_22, context_22) {
    "use strict";
    var __moduleName = context_22 && context_22.id;
    function default_19(compare) {
        if (!compare)
            compare = ascending;
        function compareNode(a, b) {
            return a && b ? compare(a.__data__, b.__data__) : !a - !b;
        }
        for (var groups = this._groups, m = groups.length, sortgroups = new Array(m), j = 0; j < m; ++j) {
            for (var group = groups[j], n = group.length, sortgroup = sortgroups[j] = new Array(n), node, i = 0; i < n; ++i) {
                if (node = group[i]) {
                    sortgroup[i] = node;
                }
            }
            sortgroup.sort(compareNode);
        }
        return new index_8.Selection(sortgroups, this._parents).order();
    }
    exports_22("default", default_19);
    function ascending(a, b) {
        return a < b ? -1 : a > b ? 1 : a >= b ? 0 : NaN;
    }
    var index_8;
    return {
        setters: [
            function (index_8_1) {
                index_8 = index_8_1;
            }
        ],
        execute: function () {
        }
    };
});
System.register("d3-selection/src/selection/call", [], function (exports_23, context_23) {
    "use strict";
    var __moduleName = context_23 && context_23.id;
    function default_20() {
        var callback = arguments[0];
        arguments[0] = this;
        callback.apply(null, arguments);
        return this;
    }
    exports_23("default", default_20);
    return {
        setters: [],
        execute: function () {
        }
    };
});
System.register("d3-selection/src/selection/nodes", [], function (exports_24, context_24) {
    "use strict";
    var __moduleName = context_24 && context_24.id;
    function default_21() {
        var nodes = new Array(this.size()), i = -1;
        this.each(function () { nodes[++i] = this; });
        return nodes;
    }
    exports_24("default", default_21);
    return {
        setters: [],
        execute: function () {
        }
    };
});
System.register("d3-selection/src/selection/node", [], function (exports_25, context_25) {
    "use strict";
    var __moduleName = context_25 && context_25.id;
    function default_22() {
        for (var groups = this._groups, j = 0, m = groups.length; j < m; ++j) {
            for (var group = groups[j], i = 0, n = group.length; i < n; ++i) {
                var node = group[i];
                if (node)
                    return node;
            }
        }
        return null;
    }
    exports_25("default", default_22);
    return {
        setters: [],
        execute: function () {
        }
    };
});
System.register("d3-selection/src/selection/size", [], function (exports_26, context_26) {
    "use strict";
    var __moduleName = context_26 && context_26.id;
    function default_23() {
        var size = 0;
        this.each(function () { ++size; });
        return size;
    }
    exports_26("default", default_23);
    return {
        setters: [],
        execute: function () {
        }
    };
});
System.register("d3-selection/src/selection/empty", [], function (exports_27, context_27) {
    "use strict";
    var __moduleName = context_27 && context_27.id;
    function default_24() {
        return !this.node();
    }
    exports_27("default", default_24);
    return {
        setters: [],
        execute: function () {
        }
    };
});
System.register("d3-selection/src/selection/each", [], function (exports_28, context_28) {
    "use strict";
    var __moduleName = context_28 && context_28.id;
    function default_25(callback) {
        for (var groups = this._groups, j = 0, m = groups.length; j < m; ++j) {
            for (var group = groups[j], i = 0, n = group.length, node; i < n; ++i) {
                if (node = group[i])
                    callback.call(node, node.__data__, i, group);
            }
        }
        return this;
    }
    exports_28("default", default_25);
    return {
        setters: [],
        execute: function () {
        }
    };
});
System.register("d3-selection/src/selection/attr", ["d3-selection/src/namespace"], function (exports_29, context_29) {
    "use strict";
    var __moduleName = context_29 && context_29.id;
    function attrRemove(name) {
        return function () {
            this.removeAttribute(name);
        };
    }
    function attrRemoveNS(fullname) {
        return function () {
            this.removeAttributeNS(fullname.space, fullname.local);
        };
    }
    function attrConstant(name, value) {
        return function () {
            this.setAttribute(name, value);
        };
    }
    function attrConstantNS(fullname, value) {
        return function () {
            this.setAttributeNS(fullname.space, fullname.local, value);
        };
    }
    function attrFunction(name, value) {
        return function () {
            var v = value.apply(this, arguments);
            if (v == null)
                this.removeAttribute(name);
            else
                this.setAttribute(name, v);
        };
    }
    function attrFunctionNS(fullname, value) {
        return function () {
            var v = value.apply(this, arguments);
            if (v == null)
                this.removeAttributeNS(fullname.space, fullname.local);
            else
                this.setAttributeNS(fullname.space, fullname.local, v);
        };
    }
    function default_26(name, value) {
        var fullname = namespace_2.default(name);
        if (arguments.length < 2) {
            var node = this.node();
            return fullname.local
                ? node.getAttributeNS(fullname.space, fullname.local)
                : node.getAttribute(fullname);
        }
        return this.each((value == null
            ? (fullname.local ? attrRemoveNS : attrRemove) : (typeof value === "function"
            ? (fullname.local ? attrFunctionNS : attrFunction)
            : (fullname.local ? attrConstantNS : attrConstant)))(fullname, value));
    }
    exports_29("default", default_26);
    var namespace_2;
    return {
        setters: [
            function (namespace_2_1) {
                namespace_2 = namespace_2_1;
            }
        ],
        execute: function () {
        }
    };
});
System.register("d3-selection/src/window", [], function (exports_30, context_30) {
    "use strict";
    var __moduleName = context_30 && context_30.id;
    function default_27(node) {
        return (node.ownerDocument && node.ownerDocument.defaultView) // node is a Node
            || (node.document && node) // node is a Window
            || node.defaultView; // node is a Document
    }
    exports_30("default", default_27);
    return {
        setters: [],
        execute: function () {
        }
    };
});
System.register("d3-selection/src/selection/style", ["d3-selection/src/window"], function (exports_31, context_31) {
    "use strict";
    var __moduleName = context_31 && context_31.id;
    function styleRemove(name) {
        return function () {
            this.style.removeProperty(name);
        };
    }
    function styleConstant(name, value, priority) {
        return function () {
            this.style.setProperty(name, value, priority);
        };
    }
    function styleFunction(name, value, priority) {
        return function () {
            var v = value.apply(this, arguments);
            if (v == null)
                this.style.removeProperty(name);
            else
                this.style.setProperty(name, v, priority);
        };
    }
    function default_28(name, value, priority) {
        var node;
        return arguments.length > 1
            ? this.each((value == null
                ? styleRemove : typeof value === "function"
                ? styleFunction
                : styleConstant)(name, value, priority == null ? "" : priority))
            : window_1.default(node = this.node())
                .getComputedStyle(node, null)
                .getPropertyValue(name);
    }
    exports_31("default", default_28);
    var window_1;
    return {
        setters: [
            function (window_1_1) {
                window_1 = window_1_1;
            }
        ],
        execute: function () {
        }
    };
});
System.register("d3-selection/src/selection/property", [], function (exports_32, context_32) {
    "use strict";
    var __moduleName = context_32 && context_32.id;
    function propertyRemove(name) {
        return function () {
            delete this[name];
        };
    }
    function propertyConstant(name, value) {
        return function () {
            this[name] = value;
        };
    }
    function propertyFunction(name, value) {
        return function () {
            var v = value.apply(this, arguments);
            if (v == null)
                delete this[name];
            else
                this[name] = v;
        };
    }
    function default_29(name, value) {
        return arguments.length > 1
            ? this.each((value == null
                ? propertyRemove : typeof value === "function"
                ? propertyFunction
                : propertyConstant)(name, value))
            : this.node()[name];
    }
    exports_32("default", default_29);
    return {
        setters: [],
        execute: function () {
        }
    };
});
System.register("d3-selection/src/selection/classed", [], function (exports_33, context_33) {
    "use strict";
    var __moduleName = context_33 && context_33.id;
    function classArray(string) {
        return string.trim().split(/^|\s+/);
    }
    function classList(node) {
        return node.classList || new ClassList(node);
    }
    function ClassList(node) {
        this._node = node;
        this._names = classArray(node.getAttribute("class") || "");
    }
    function classedAdd(node, names) {
        var list = classList(node), i = -1, n = names.length;
        while (++i < n)
            list.add(names[i]);
    }
    function classedRemove(node, names) {
        var list = classList(node), i = -1, n = names.length;
        while (++i < n)
            list.remove(names[i]);
    }
    function classedTrue(names) {
        return function () {
            classedAdd(this, names);
        };
    }
    function classedFalse(names) {
        return function () {
            classedRemove(this, names);
        };
    }
    function classedFunction(names, value) {
        return function () {
            (value.apply(this, arguments) ? classedAdd : classedRemove)(this, names);
        };
    }
    function default_30(name, value) {
        var names = classArray(name + "");
        if (arguments.length < 2) {
            var list = classList(this.node()), i = -1, n = names.length;
            while (++i < n)
                if (!list.contains(names[i]))
                    return false;
            return true;
        }
        return this.each((typeof value === "function"
            ? classedFunction : value
            ? classedTrue
            : classedFalse)(names, value));
    }
    exports_33("default", default_30);
    return {
        setters: [],
        execute: function () {
            ClassList.prototype = {
                add: function (name) {
                    var i = this._names.indexOf(name);
                    if (i < 0) {
                        this._names.push(name);
                        this._node.setAttribute("class", this._names.join(" "));
                    }
                },
                remove: function (name) {
                    var i = this._names.indexOf(name);
                    if (i >= 0) {
                        this._names.splice(i, 1);
                        this._node.setAttribute("class", this._names.join(" "));
                    }
                },
                contains: function (name) {
                    return this._names.indexOf(name) >= 0;
                }
            };
        }
    };
});
System.register("d3-selection/src/selection/text", [], function (exports_34, context_34) {
    "use strict";
    var __moduleName = context_34 && context_34.id;
    function textRemove() {
        this.textContent = "";
    }
    function textConstant(value) {
        return function () {
            this.textContent = value;
        };
    }
    function textFunction(value) {
        return function () {
            var v = value.apply(this, arguments);
            this.textContent = v == null ? "" : v;
        };
    }
    function default_31(value) {
        return arguments.length
            ? this.each(value == null
                ? textRemove : (typeof value === "function"
                ? textFunction
                : textConstant)(value))
            : this.node().textContent;
    }
    exports_34("default", default_31);
    return {
        setters: [],
        execute: function () {
        }
    };
});
System.register("d3-selection/src/selection/html", [], function (exports_35, context_35) {
    "use strict";
    var __moduleName = context_35 && context_35.id;
    function htmlRemove() {
        this.innerHTML = "";
    }
    function htmlConstant(value) {
        return function () {
            this.innerHTML = value;
        };
    }
    function htmlFunction(value) {
        return function () {
            var v = value.apply(this, arguments);
            this.innerHTML = v == null ? "" : v;
        };
    }
    function default_32(value) {
        return arguments.length
            ? this.each(value == null
                ? htmlRemove : (typeof value === "function"
                ? htmlFunction
                : htmlConstant)(value))
            : this.node().innerHTML;
    }
    exports_35("default", default_32);
    return {
        setters: [],
        execute: function () {
        }
    };
});
System.register("d3-selection/src/selection/raise", [], function (exports_36, context_36) {
    "use strict";
    var __moduleName = context_36 && context_36.id;
    function raise() {
        if (this.nextSibling)
            this.parentNode.appendChild(this);
    }
    function default_33() {
        return this.each(raise);
    }
    exports_36("default", default_33);
    return {
        setters: [],
        execute: function () {
        }
    };
});
System.register("d3-selection/src/selection/lower", [], function (exports_37, context_37) {
    "use strict";
    var __moduleName = context_37 && context_37.id;
    function lower() {
        if (this.previousSibling)
            this.parentNode.insertBefore(this, this.parentNode.firstChild);
    }
    function default_34() {
        return this.each(lower);
    }
    exports_37("default", default_34);
    return {
        setters: [],
        execute: function () {
        }
    };
});
System.register("d3-selection/src/selection/append", ["d3-selection/src/creator"], function (exports_38, context_38) {
    "use strict";
    var __moduleName = context_38 && context_38.id;
    function default_35(name) {
        var create = typeof name === "function" ? name : creator_1.default(name);
        return this.select(function () {
            return this.appendChild(create.apply(this, arguments));
        });
    }
    exports_38("default", default_35);
    var creator_1;
    return {
        setters: [
            function (creator_1_1) {
                creator_1 = creator_1_1;
            }
        ],
        execute: function () {
        }
    };
});
System.register("d3-selection/src/selection/insert", ["d3-selection/src/creator", "d3-selection/src/selector"], function (exports_39, context_39) {
    "use strict";
    var __moduleName = context_39 && context_39.id;
    function constantNull() {
        return null;
    }
    function default_36(name, before) {
        var create = typeof name === "function" ? name : creator_2.default(name), select = before == null ? constantNull : typeof before === "function" ? before : selector_2.default(before);
        return this.select(function () {
            return this.insertBefore(create.apply(this, arguments), select.apply(this, arguments) || null);
        });
    }
    exports_39("default", default_36);
    var creator_2, selector_2;
    return {
        setters: [
            function (creator_2_1) {
                creator_2 = creator_2_1;
            },
            function (selector_2_1) {
                selector_2 = selector_2_1;
            }
        ],
        execute: function () {
        }
    };
});
System.register("d3-selection/src/selection/remove", [], function (exports_40, context_40) {
    "use strict";
    var __moduleName = context_40 && context_40.id;
    function remove() {
        var parent = this.parentNode;
        if (parent)
            parent.removeChild(this);
    }
    function default_37() {
        return this.each(remove);
    }
    exports_40("default", default_37);
    return {
        setters: [],
        execute: function () {
        }
    };
});
System.register("d3-selection/src/selection/datum", [], function (exports_41, context_41) {
    "use strict";
    var __moduleName = context_41 && context_41.id;
    function default_38(value) {
        return arguments.length
            ? this.property("__data__", value)
            : this.node().__data__;
    }
    exports_41("default", default_38);
    return {
        setters: [],
        execute: function () {
        }
    };
});
System.register("d3-selection/src/selection/dispatch", ["d3-selection/src/window"], function (exports_42, context_42) {
    "use strict";
    var __moduleName = context_42 && context_42.id;
    function dispatchEvent(node, type, params) {
        var window = window_2.default(node), event = window.CustomEvent;
        if (event) {
            event = new event(type, params);
        }
        else {
            event = window.document.createEvent("Event");
            if (params)
                event.initEvent(type, params.bubbles, params.cancelable), event.detail = params.detail;
            else
                event.initEvent(type, false, false);
        }
        node.dispatchEvent(event);
    }
    function dispatchConstant(type, params) {
        return function () {
            return dispatchEvent(this, type, params);
        };
    }
    function dispatchFunction(type, params) {
        return function () {
            return dispatchEvent(this, type, params.apply(this, arguments));
        };
    }
    function default_39(type, params) {
        return this.each((typeof params === "function"
            ? dispatchFunction
            : dispatchConstant)(type, params));
    }
    exports_42("default", default_39);
    var window_2;
    return {
        setters: [
            function (window_2_1) {
                window_2 = window_2_1;
            }
        ],
        execute: function () {
        }
    };
});
System.register("d3-selection/src/selection/index", ["d3-selection/src/selection/select", "d3-selection/src/selection/selectAll", "d3-selection/src/selection/filter", "d3-selection/src/selection/data", "d3-selection/src/selection/enter", "d3-selection/src/selection/exit", "d3-selection/src/selection/merge", "d3-selection/src/selection/order", "d3-selection/src/selection/sort", "d3-selection/src/selection/call", "d3-selection/src/selection/nodes", "d3-selection/src/selection/node", "d3-selection/src/selection/size", "d3-selection/src/selection/empty", "d3-selection/src/selection/each", "d3-selection/src/selection/attr", "d3-selection/src/selection/style", "d3-selection/src/selection/property", "d3-selection/src/selection/classed", "d3-selection/src/selection/text", "d3-selection/src/selection/html", "d3-selection/src/selection/raise", "d3-selection/src/selection/lower", "d3-selection/src/selection/append", "d3-selection/src/selection/insert", "d3-selection/src/selection/remove", "d3-selection/src/selection/datum", "d3-selection/src/selection/on", "d3-selection/src/selection/dispatch"], function (exports_43, context_43) {
    "use strict";
    var __moduleName = context_43 && context_43.id;
    function Selection(groups, parents) {
        this._groups = groups;
        this._parents = parents;
    }
    exports_43("Selection", Selection);
    function selection() {
        return new Selection([[document.documentElement]], root);
    }
    var select_1, selectAll_1, filter_1, data_1, enter_2, exit_1, merge_1, order_1, sort_1, call_1, nodes_1, node_1, size_1, empty_1, each_1, attr_1, style_1, property_1, classed_1, text_1, html_1, raise_1, lower_1, append_1, insert_1, remove_1, datum_1, on_2, dispatch_1, root;
    return {
        setters: [
            function (select_1_1) {
                select_1 = select_1_1;
            },
            function (selectAll_1_1) {
                selectAll_1 = selectAll_1_1;
            },
            function (filter_1_1) {
                filter_1 = filter_1_1;
            },
            function (data_1_1) {
                data_1 = data_1_1;
            },
            function (enter_2_1) {
                enter_2 = enter_2_1;
            },
            function (exit_1_1) {
                exit_1 = exit_1_1;
            },
            function (merge_1_1) {
                merge_1 = merge_1_1;
            },
            function (order_1_1) {
                order_1 = order_1_1;
            },
            function (sort_1_1) {
                sort_1 = sort_1_1;
            },
            function (call_1_1) {
                call_1 = call_1_1;
            },
            function (nodes_1_1) {
                nodes_1 = nodes_1_1;
            },
            function (node_1_1) {
                node_1 = node_1_1;
            },
            function (size_1_1) {
                size_1 = size_1_1;
            },
            function (empty_1_1) {
                empty_1 = empty_1_1;
            },
            function (each_1_1) {
                each_1 = each_1_1;
            },
            function (attr_1_1) {
                attr_1 = attr_1_1;
            },
            function (style_1_1) {
                style_1 = style_1_1;
            },
            function (property_1_1) {
                property_1 = property_1_1;
            },
            function (classed_1_1) {
                classed_1 = classed_1_1;
            },
            function (text_1_1) {
                text_1 = text_1_1;
            },
            function (html_1_1) {
                html_1 = html_1_1;
            },
            function (raise_1_1) {
                raise_1 = raise_1_1;
            },
            function (lower_1_1) {
                lower_1 = lower_1_1;
            },
            function (append_1_1) {
                append_1 = append_1_1;
            },
            function (insert_1_1) {
                insert_1 = insert_1_1;
            },
            function (remove_1_1) {
                remove_1 = remove_1_1;
            },
            function (datum_1_1) {
                datum_1 = datum_1_1;
            },
            function (on_2_1) {
                on_2 = on_2_1;
            },
            function (dispatch_1_1) {
                dispatch_1 = dispatch_1_1;
            }
        ],
        execute: function () {
            exports_43("root", root = [null]);
            Selection.prototype = selection.prototype = {
                constructor: Selection,
                select: select_1.default,
                selectAll: selectAll_1.default,
                filter: filter_1.default,
                data: data_1.default,
                enter: enter_2.default,
                exit: exit_1.default,
                merge: merge_1.default,
                order: order_1.default,
                sort: sort_1.default,
                call: call_1.default,
                nodes: nodes_1.default,
                node: node_1.default,
                size: size_1.default,
                empty: empty_1.default,
                each: each_1.default,
                attr: attr_1.default,
                style: style_1.default,
                property: property_1.default,
                classed: classed_1.default,
                text: text_1.default,
                html: html_1.default,
                raise: raise_1.default,
                lower: lower_1.default,
                append: append_1.default,
                insert: insert_1.default,
                remove: remove_1.default,
                datum: datum_1.default,
                on: on_2.default,
                dispatch: dispatch_1.default
            };
            exports_43("default", selection);
        }
    };
});
System.register("d3-selection/src/select", ["d3-selection/src/selection/index"], function (exports_44, context_44) {
    "use strict";
    var __moduleName = context_44 && context_44.id;
    function default_40(selector) {
        return typeof selector === "string"
            ? new index_9.Selection([[document.querySelector(selector)]], [document.documentElement])
            : new index_9.Selection([[selector]], index_9.root);
    }
    exports_44("default", default_40);
    var index_9;
    return {
        setters: [
            function (index_9_1) {
                index_9 = index_9_1;
            }
        ],
        execute: function () {
        }
    };
});
System.register("d3-selection/src/selectAll", ["d3-selection/src/selection/index"], function (exports_45, context_45) {
    "use strict";
    var __moduleName = context_45 && context_45.id;
    function default_41(selector) {
        return typeof selector === "string"
            ? new index_10.Selection([document.querySelectorAll(selector)], [document.documentElement])
            : new index_10.Selection([selector == null ? [] : selector], index_10.root);
    }
    exports_45("default", default_41);
    var index_10;
    return {
        setters: [
            function (index_10_1) {
                index_10 = index_10_1;
            }
        ],
        execute: function () {
        }
    };
});
System.register("d3-selection/src/touch", ["d3-selection/src/sourceEvent", "d3-selection/src/point"], function (exports_46, context_46) {
    "use strict";
    var __moduleName = context_46 && context_46.id;
    function default_42(node, touches, identifier) {
        if (arguments.length < 3)
            identifier = touches, touches = sourceEvent_2.default().changedTouches;
        for (var i = 0, n = touches ? touches.length : 0, touch; i < n; ++i) {
            if ((touch = touches[i]).identifier === identifier) {
                return point_2.default(node, touch);
            }
        }
        return null;
    }
    exports_46("default", default_42);
    var sourceEvent_2, point_2;
    return {
        setters: [
            function (sourceEvent_2_1) {
                sourceEvent_2 = sourceEvent_2_1;
            },
            function (point_2_1) {
                point_2 = point_2_1;
            }
        ],
        execute: function () {
        }
    };
});
System.register("d3-selection/src/touches", ["d3-selection/src/sourceEvent", "d3-selection/src/point"], function (exports_47, context_47) {
    "use strict";
    var __moduleName = context_47 && context_47.id;
    function default_43(node, touches) {
        if (touches == null)
            touches = sourceEvent_3.default().touches;
        for (var i = 0, n = touches ? touches.length : 0, points = new Array(n); i < n; ++i) {
            points[i] = point_3.default(node, touches[i]);
        }
        return points;
    }
    exports_47("default", default_43);
    var sourceEvent_3, point_3;
    return {
        setters: [
            function (sourceEvent_3_1) {
                sourceEvent_3 = sourceEvent_3_1;
            },
            function (point_3_1) {
                point_3 = point_3_1;
            }
        ],
        execute: function () {
        }
    };
});
System.register("d3-selection/index", ["d3-selection/src/creator", "d3-selection/src/local", "d3-selection/src/matcher", "d3-selection/src/mouse", "d3-selection/src/namespace", "d3-selection/src/namespaces", "d3-selection/src/select", "d3-selection/src/selectAll", "d3-selection/src/selection/index", "d3-selection/src/selector", "d3-selection/src/selectorAll", "d3-selection/src/touch", "d3-selection/src/touches", "d3-selection/src/window", "d3-selection/src/selection/on"], function (exports_48, context_48) {
    "use strict";
    var __moduleName = context_48 && context_48.id;
    return {
        setters: [
            function (creator_3_1) {
                exports_48({
                    "creator": creator_3_1["default"]
                });
            },
            function (local_1_1) {
                exports_48({
                    "local": local_1_1["default"]
                });
            },
            function (matcher_2_1) {
                exports_48({
                    "matcher": matcher_2_1["default"]
                });
            },
            function (mouse_1_1) {
                exports_48({
                    "mouse": mouse_1_1["default"]
                });
            },
            function (namespace_3_1) {
                exports_48({
                    "namespace": namespace_3_1["default"]
                });
            },
            function (namespaces_3_1) {
                exports_48({
                    "namespaces": namespaces_3_1["default"]
                });
            },
            function (select_2_1) {
                exports_48({
                    "select": select_2_1["default"]
                });
            },
            function (selectAll_2_1) {
                exports_48({
                    "selectAll": selectAll_2_1["default"]
                });
            },
            function (index_11_1) {
                exports_48({
                    "selection": index_11_1["default"]
                });
            },
            function (selector_3_1) {
                exports_48({
                    "selector": selector_3_1["default"]
                });
            },
            function (selectorAll_2_1) {
                exports_48({
                    "selectorAll": selectorAll_2_1["default"]
                });
            },
            function (touch_1_1) {
                exports_48({
                    "touch": touch_1_1["default"]
                });
            },
            function (touches_1_1) {
                exports_48({
                    "touches": touches_1_1["default"]
                });
            },
            function (window_3_1) {
                exports_48({
                    "window": window_3_1["default"]
                });
            },
            function (on_3_1) {
                exports_48({
                    "event": on_3_1["event"],
                    "customEvent": on_3_1["customEvent"]
                });
            }
        ],
        execute: function () {
        }
    };
});
System.register("src/_d3", ["d3-selection/index"], function (exports_49, context_49) {
    "use strict";
    var __moduleName = context_49 && context_49.id;
    var index_js_1;
    return {
        setters: [
            function (index_js_1_1) {
                index_js_1 = index_js_1_1;
            }
        ],
        execute: function () {
            exports_49("default", {
                select: index_js_1.select,
            });
        }
    };
});
System.register("src/_graph", ["src/_d3"], function (exports_50, context_50) {
    "use strict";
    var __moduleName = context_50 && context_50.id;
    var _d3_1, graph;
    return {
        setters: [
            function (_d3_1_1) {
                _d3_1 = _d3_1_1;
            }
        ],
        execute: function () {
            graph = function () {
                var svg = _d3_1.default.select("svg");
                svg.append("circle")
                    .attr("r", 20)
                    .attr("cx", 20)
                    .attr("cy", 20);
            };
            exports_50("default", graph);
        }
    };
});
System.register("src/main", ["src/_graph"], function (exports_51, context_51) {
    "use strict";
    var __moduleName = context_51 && context_51.id;
    var _graph_1;
    return {
        setters: [
            function (_graph_1_1) {
                _graph_1 = _graph_1_1;
            }
        ],
        execute: function () {
            _graph_1.default();
        }
    };
});
//# sourceMappingURL=main.ts.js.map