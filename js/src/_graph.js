import d3 from "./_d3";
let graph = () => {
	let svg = d3.select("svg");
	svg.append("circle")
		.attr("r", 20)
		.attr("cx", 20)
		.attr("cy", 20);
};

export default graph;

